<!DOCTYPE html>
<head>
  <title>Relojoaria Seiko</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <link href="css-site/css-site.css" rel="stylesheet" type="text/css"/>
  <style>
  /* Make the image fully responsive */
  .carousel-inner img {
      width: 300px;
      height: 300px;
  }
  </style>

</head>
<body>
 <?php include_once 'dao/relogioDAO.php'; ?>
    
    <div class="container-fluid">
        <div class="row menu-teste">
            <img class="tituloImagen img-fluid" src="/imgs/logo.png" alt="Logo">

      <nav class="navbar navbar-expand-sm ">
    <ul class="navbar-nav">
    <li class="nav-item">
        <a class="nav-link txt-titulo col-3" href="index.php">Home</a>
    </li>
    <li class="nav-item">
        <a class="nav-link txt-titulo col-3" href="RelogioMasculino.php">Relogios Masculinos</a>
    </li>
    <li class="nav-item">
        <a class="nav-link txt-titulo col-3" href="RelogioFeminino.php">Relogios Femininos</a>
    </li>
     <li class="nav-item">
      <a class="nav-link txt-titulo col-3" href="CRUD_relogios.php">Cadastrar</a>
    </li>
  </ul>
</nav>
                
        </div>
<div id="demo" class="carousel slide" data-ride="carousel">

  <!-- Indicators -->
  <ul class="carousel-indicators">
    <li data-target="#demo" data-slide-to="0" class="active"></li>
    <li data-target="#demo" data-slide-to="1"></li>
    <li data-target="#demo" data-slide-to="2"></li>
  </ul>
  
  <!-- The slideshow -->
  <div class="carousel-inner teste">
    <div class="carousel-item active">
      <img src="/imgs/relogioMas1.png" alt="Relogio1" width="1100" height="500">
    </div>
    <div class="carousel-item">
      <img src="/imgs/relogioMas2.png" alt="Relogio2" width="1100" height="500">
    </div>
    <div class="carousel-item">
      <img src="/imgs/relogioMas3.png" alt="Relogio3" width="1100" height="500">
    </div>
  </div>
  
  <!-- Left and right controls -->
  <a class="carousel-control-prev" href="#demo" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#demo" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>
</div>
    <div class="container">
    <div class="row">
        <div class="col-12">
            <p class="titulo2">Modelos de Relógios</p>
        </div>
        <?php 
            $reloDAO = new relogioDAO();
            $relogios =$reloDAO->buscar();

            
            
            foreach ($relogios as $item){
            ?>
        <div class="col-3 blocos-produto">
            <p class="txt-produto1"><?php echo $item->nome; ?></p>
            <p class="txt-produto1"><?php echo $item->codigo; ?></p>
            <img src="<?php echo $item->end_img ?>" class="img-fluid" alt="Relogio1">
            <p class="txt-produto1">Preço:<?php echo $item->preco ?></p>
        </div>
            <?php }?>
                
        <div class="col-12">
            <div class="txt-cusor">
            <a href="#"> <    </a>
            <a href="#"> 1 </a>
            <a href="#"> 2 </a>
            <a href="#"> 3 </a>
            <a href="#"> ...</a>
            <a href="#">      > </a>
            </div>
        </div>
         <div class="col-12">
            <p class="titulo2">Endereço</p>
        </div>
         <div class="col-12">
            <p class="txt-contato">Rua: Joaquim Murtinho, 109 Loja A - Cep 38600-206</p>
            <p class="txt-contato">Ponto de Referencia: LOJA NOVO MUNDO</p>
            
        </div>
        <div class="col-12">
            <p class="titulo2">Contato</p>
        </div>
         <div class="col-12">
            <p class="txt-contato">Cel. Igor: (38) 99912-4757</p>
            <p class="txt-contato">Cel. Ramon: (38) 99912-4757</p>
            
        </div>
    </div>
        </div>
</body>
</html>

<!DOCTYPE html>
<head>
  <title>Relojoaria Seiko</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <link href="css-site/css-site.css" rel="stylesheet" type="text/css"/>
 

</head>
<body>
 <?php include_once 'dao/relogioDAO.php'; ?>
    
    <div class="container-fluid">
        <div class="row menu-teste">
            <img class="tituloImagen img-fluid" src="/imgs/logo.png" alt="Logo">

      <nav class="navbar navbar-expand-sm  ">
    <ul class="navbar-nav">
    <li class="nav-item">
        <a class="nav-link txt-titulo col-3" href="index.php">Home</a>
    </li>
    <li class="nav-item">
        <a class="nav-link txt-titulo col-3" href="RelogioMasculino.php">Relogios Masculinos</a>
    </li>
    <li class="nav-item">
        <a class="nav-link txt-titulo col-3" href="RelogioFeminino.php">Relogios Femininos</a>
    </li>
     <li class="nav-item">
      <a class="nav-link txt-titulo col-3" href="CRUD_relogios.php">Cadastrar</a>
    </li>
  </ul>
</nav>
                
        </div>
    </div>
    

    <div class="container">
    <div class="row">
        <div class="col-12">
            <p class="titulo2">Modelos de Relógios Femininos</p>
        </div>
        <?php 
            $reloDAO = new relogioDAO();
            $relogios =$reloDAO->buscarFeminino();

            
            
            foreach ($relogios as $item){
            ?>
        <div class="col-3 blocos-produto">
            <p class="txt-produto1"><?php echo $item->nome; ?></p>
            <p class="txt-produto1"><?php echo $item->codigo; ?></p>
            <img src="<?php echo $item->end_img ?>" class="img-fluid" alt="Relogio1">
            <p class="txt-produto1">Preço:<?php echo $item->preco ?></p>
        </div>
            <?php }?>
                
        <div class="col-12">
            <div class="txt-cusor">
            <a href="#"> <    </a>
            <a href="#"> 1 </a>
            <a href="#"> 2 </a>
            <a href="#"> 3 </a>
            <a href="#"> ...</a>
            <a href="#">      > </a>
            </div>
        </div>
         <div class="col-12">
            <p class="titulo2">Endereço</p>
        </div>
         <div class="col-12">
            <p class="txt-contato">Rua: Joaquim Murtinho, 109 Loja A - Cep 38600-206</p>
            <p class="txt-contato">Ponto de Referencia: LOJA NOVO MUNDO</p>
            
        </div>
        <div class="col-12">
            <p class="titulo2"></p>
        </div>
        
    </div>
        </div>
</body>
</html>

<?php 
require_once '../dao/relogioDAO.php';
$reloDAO = new relogioDAO();
$relogio = $reloDAO->buscarRegistro($_POST['id']);
$relogio = $relogio[0];

?>
<!DOCTYPE html>
<head>
  <title>Teste PHP</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <link href="../css-site/css-manter-site.css" rel="stylesheet" type="text/css"/>
</head>
<body>
    <script type="text/javascript">
function conf(){ 
   if( !confirm("Tem certeza que deseja alterar esse cadastro?") )
           return false;
}
</script>
<div class="container">
 <div class="tituloCad">
        <h3>Alterar Relógio</h3>
    </div>
    
<div class="row justify-content-center">
    <form onsubmit="conf()" action="/Controle/ControleRelogios.php"  method="POST">
            <input type="hidden" name="acao" value="alterar">
            <input type="hidden" name="id" value="<?php echo $relogio->id ?>">
            <div class="form-group">
            <label>Nome:</label>
            <input class="form-control" type="text" name="nome" value="<?php echo $relogio->nome; ?>">
            </div>
            <div class="form-group">
            <label>Código:</label>
            <input class="form-control" type="text" name="codigo" value="<?php echo $relogio->codigo; ?>">
            </div>
            <div class="form-group">
            <label>Preço:</label>
            <input class="form-control" type="text" name="preco" value="<?php echo $relogio->preco; ?>">
            </div>
            <div class="form-group">
            <label>Caminho da imagem:</label>
            <input class="form-control" type="text" name="end-img" value="<?php echo $relogio->end_img; ?>">
            </div>
            <div class="form-group">
            <button class="btn btn-primary" type="submit" name="cadastrar">Salvar</button>
            </div>
        </form>
            </div>
    </div>
</body>
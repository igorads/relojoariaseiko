<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ControleRelogios
 *
 * @author root
 */
ini_set('display_errors', 1);
$acao = $_POST['acao'];
require_once '../dao/relogioDAO.php';
$reloDAO = new relogioDAO();
if(!isset($_SESSION)){
    session_start();
}

switch ($acao){
    case 'inserir':
        validaDados();
        header('Location: ../CRUD_relogios.php');
        break;
    case 'excluir':
        excluir($_POST['id']);
        header('Location: ../CRUD_relogios.php');
        break;
    case 'alterar':
        validaDadosAlterar();
        header('Location: ../CRUD_relogios.php');
         break;   
    default:
        echo 'deuerrado';
}

function validaDados(){
    $nome = $_POST['nome'];
    $codigo = $_POST['codigo'];
    $preco = $_POST['preco'];
    $end_img = $_POST['end-img'];
    if($nome != null){
        if($codigo != null){
            if($preco != null){
                if($end_img != null){
                    $relogio = array(
                        "nome" => $nome,
                        "codigo" => $codigo,
                        "preco" => $preco,
                        "end_img" => $end_img,  
                    );
                    $reloDAO = new relogioDAO();
                    $msm=$reloDAO->inserir($relogio);
                    print_r($msm);
                  if($msm){
                       $_SESSION['message'] = "Cadastrado realizado com sucesso.";
                       $_SESSION['msg_type'] = "success";
                   }
                   else{
                       $_SESSION['message'] = "Cadastrado não realizado.";
                       $_SESSION['msg_type'] = "danger";
                   }
            }
            else
             echo 'Digite o endereço da imagem corretamente.';   
        }
        else
            echo 'Digite o preço do relógio.';  
    }
    else{
        echo 'Digite o código do relógio.';
    }
}
else{
    echo 'Digite o nome do relógio.';
}
}

function validaDadosAlterar(){
    $nome = $_POST['nome'];
    $codigo = $_POST['codigo'];
    $preco = $_POST['preco'];
    $end_img = $_POST['end-img'];
    
    if($nome != null){
        if($codigo != null){
            if($preco != null){
                if($end_img != null){
                    $relogio = array(
                        "nome" => $nome,
                        "codigo" => $codigo,
                        "preco" => $preco,
                        "end_img" => $end_img,  
                    );
                   print_r($relogio);
                    $dao = new relogioDAO();
                   
                   $msm=$dao->alterar($relogio, $_POST['id']);
                   if($msm){
                       $_SESSION['message'] = "Cadastrado atualizado com sucesso.";
                       $_SESSION['msg_type'] = "success";
                   }
                   else{
                       $_SESSION['message'] = "Cadastrado não atualizado realizado.";
                       $_SESSION['msg_type'] = "danger";
                   }
            }
            else
             echo 'Digite o endereço da imagem corretamente.';   
        }
        else
            echo 'Digite o preço do relógio.';  
    }
    else{
        echo 'Digite o código do relógio.';
    }
}
else{
    echo 'Digite o nome do relógio.';
}
}

 function excluir($id){
   $dao = new relogioDAO();
   $msg=$dao->excluir($id);
   print_r($msg);
   if($msg){
             $_SESSION['message'] = "Cadastro excluído com sucesso.";
             $_SESSION['msg_type'] = "success";
              }
              else{
                 $_SESSION['message'] = "Cadastro não foi excluído.";
                 $_SESSION['msg_type'] = "danger";
                   }
}


?>
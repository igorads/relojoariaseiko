<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Conexao
 *
 * @author root
 */
    class Conexao
{

    private static $instancia;

    private function __construct()
    { }

    public static function getInstancia()
    {
        if (!isset(self::$instancia)) {
            self::$instancia = new PDO(
                "mysql:host=localhost;dbname=ppi;",
                "root",
                "0670",
                [
                    PDO::MYSQL_ATTR_INIT_COMMAND =>
                    'set names utf8',
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                    PDO::ATTR_ORACLE_NULLS => PDO::NULL_EMPTY_STRING
                ]
            );
        }
        return self::$instancia;
    }
}

?>

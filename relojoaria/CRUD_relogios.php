<!DOCTYPE html>
<head>
  <title>Manter Relógio</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <link href="css-site/css-manter-site.css" rel="stylesheet" type="text/css"/>
</head>
<body>

    <?php require_once 'dao/relogioDAO.php'?>;
    
    <?php 
        if(!isset($_SESSION)){
            session_start();
            $_SESSION['mesage']="success";
        }
    ?>
        
        <?php 
    
        if(isset($_SESSION['message'])): ?>
    
    <div class="alert alert-<?=$_SESSION['msg_type']?>">
        <?php 
        echo $_SESSION['message'];
        unset($_SESSION['message']);
        ?>
        </div>
        <?php    endif; ?>
    
<script type="text/javascript">
function conf(){ 
   if( !confirm("Tem certeza que deseja excluir esse cadastro?") )
           return false;
}
</script> 
<div class="container">
    <div class="tituloCad">
        <h3>Manutenção dos Relógios</h3>
    </div>
    <table class="table">
        <thead>
        <tr>
            <th>
                ID
            </th>
            <th>
                Nome
            </th>
            <th>
                Código
            </th>
            <th>
                Preço
            </th>
            <th>
                End. Imagem
            </th>
            <th>
                <p style="float: right" >Ação</p>
            </th>
        </tr>            
            </thead>
            <tbody>
        <?php 
            $reloDAO = new relogioDAO();
            $relogios =$reloDAO->buscar();
            
            
            
            
            foreach ($relogios as $item){
            ?>
        <tr>
            <td>
                <?php echo $item->id; ?>
            </td>
            <td>
                <?php echo $item->nome; ?>
            </td>
            <td>
                <?php echo $item->codigo; ?>
            </td>
            <td>
                <?php echo $item->preco; ?>
            </td>
            <td>
                <?php echo $item->end_img; ?>
            </td>
            <td>
                <form  style="float: right" class="form-inline" action="/Controle/ControleRelogios.php"  method="POST" onsubmit="return conf()">
                <input type="hidden"  name="acao" value="excluir">
                <input type="hidden" name="id" value="<?php echo $item->id ?>">
                <button type="submit" class="btn btn-danger" name="excluir"  >Excluir</button>
                </form>
                
                <form style="float: right" class="form-inline" action="/Controle/EditarRelogio.php"  method="POST">
                <input type="hidden" name="acao" value="editar">
                <input type="hidden" name="id" value="<?php echo $item->id ?>">
                <button type="submit" name="editar" class="btn btn-warning" >Editar</button>
                </form>
                
            </td>

        </tr>
            <?php }?>
        </tbody>
    </table>   
        
    <div class="tituloCad">
        <h3>Cadastro</h3>
    </div>
    
    <div class="row justify-content-center">
            <form action="/Controle/ControleRelogios.php"  method="POST">
            <input type="hidden" name="acao" value="inserir">
            <div class="form-group">
            <label>Nome:</label>
            <input class="form-control" type="text" name="nome" placeholder="Nome">
            </div>
            <div class="form-group">
            <label>Código:</label>
            <input class="form-control" type="text" name="codigo" placeholder="Código">
            </div>
            <div class="form-group">
            <label>Preço:</label>
            <input class="form-control" type="text" name="preco" placeholder="Preço">
            </div>
            <div class="form-group">
            <label>Caminho da imagem:</label>
            <input class="form-control" type="text" name="end-img" placeholder="Caminho da imagem.">
            </div>
            <div class="form-group">
            <button class="btn btn-primary" type="submit" name="cadastrar">Cadastrar</button>
            <button onclick="history.go(-1);" class="btn btn-info" name="cadastrar">Voltar</button>
            </div>
        </form>
            </div>
    </div>
</body>
